import inspect
import sys


class error(Exception):
    pass


class NotImplemented(Exception):
    def __init__(self):
            function_name = inspect.stack()[1][3]
            message = "\n\n\tgdbstub only provides stubs and documentation. Use unittest.Mock to provide an implementation.\n"
            message += "\tNo implementation provided for `gdb.{}()`".format(function_name)
            super().__init__(message)


def fail_if_used(val=None):
    if __name__ not in sys.modules:
        return val  # try not to fail if we are being loaded
    else:
        raise NotImplemented
