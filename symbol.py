import gdbstub.exceptions as exceptions
from gdbstub.value import Value

class Symbol:
    """GDB represents every variable, function and type as an entry in a symbol table. See Examining the Symbol Table.
    Similarly, Python represents these symbols in GDB with the gdb.Symbol object.
    """

    def __init__(self):
        """gdb.Symbol cannot be instanced"""

        """
        The type of the symbol or None if no type is recorded. This attribute is represented as a gdb.Type object.
        See Types In Python. This attribute is not writable.
        """
        self.type = None

        """
        The symbol table in which the symbol appears. This attribute is represented as a gdb.Symtab object. 
        See Symbol Tables In Python. This attribute is not writable.
        """
        self.symtab = None

        """The line number in the source code at which the symbol was defined. This is an integer."""
        self.line = None

        """The name of the symbol as a string. This attribute is not writable."""
        self.name = None

        """The name of the symbol, as used by the linker (i.e., may be mangled). This attribute is not writable."""
        self.linkage_name = None

        """
        The name of the symbol in a form suitable for output. This is either name or linkage_name, depending on 
        whether the user asked GDB to display demangled or mangled names.
        """
        self.print_name = None

        """
        The address class of the symbol. This classifies how to find the value of a symbol. Each address class is a 
        constant defined in the gdb module and described later in this chapter.
        """
        self.addr_class = None

        """
        This is True if evaluating this symbol’s value requires a frame (see Frames In Python) and False otherwise. 
        Typically, local variables will require a frame, but other symbols will not.
        """
        self.needs_frame = None

        """True if the symbol is an argument of a function."""
        self.is_argument = None

        """True if the symbol is a constant."""
        self.is_constant = None

        """True if the symbol is a function or a method."""
        self.is_function = None

        """True if the symbol is a variable."""
        self.is_variable = None

    def is_valid(self):
        """
        Returns True if the gdb.Symbol object is valid, False if not. A gdb.Symbol object can become invalid if the
        symbol it refers to does not exist in GDB any longer. All other gdb.Symbol methods will throw an exception if
        it is invalid at the time the method is called.
        """
        return exceptions.fail_if_used(False)

    def value(self):
        """
        Compute the value of the symbol, as a gdb.Value. For functions, this computes the address of the function,
        cast to the appropriate type. If the symbol requires a frame in order to compute its value, then frame must be
        given. If frame is not given, or if frame is invalid, then this method will throw an exception.
        """

        return exceptions.fail_if_used(Value())



