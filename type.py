
class Type:
    """ GDB represents types from the inferior using the class gdb.Type.

    If the type is a structure or class type, or an enum type, the fields of that type can be accessed using the Python dictionary syntax.
    For example, if some_type is a gdb.Type instance holding a structure type, you can access its foo field with:
        bar = some_type['foo']
    bar will be a gdb.Field object; see below under the description of the Type.fields method for a description of the gdb.Field class.
    """

    def __init__(self, code=None, name=None, sizeof=None, tag=None):
        """gdb.Type cannot be instanced"""

        """The type code for this type. The type code will be one of the TYPE_CODE_ constants defined below."""
        self.code = code

        """The name of this type. If this type has no name, then None is returned."""
        self.name = name

        """
        The size of this type, in target char units. Usually, a target’s char type will be an 8-bit byte. 
        However, on some unusual platforms, this type may have a different size.
        """
        self.sizeof = sizeof

        """
        The tag name for this type. The tag name is the name after struct, union, or enum in C and C++; not all 
        languages have this concept. If this type has no tag name, then None is returned.
        """
        self.tag = tag

    def fields(self):
        """
        For structure and union types, this method returns the fields. Range types have two fields, the minimum and
        maximum values. Enum types have one field per enum constant. Function and method types have one field per
        parameter. The base types of C++ classes are also represented as fields. If the type has no fields, or does not
        fit into one of these categories, an empty sequence will be returned.

        Each field is a gdb.Field object, with some pre-defined attributes:
            bitpos
                This attribute is not available for enum or static (as in C++) fields. The value is the position,
                counting in bits, from the start of the containing type.

            enumval
                This attribute is only available for enum fields, and its value is the enumeration member’s integer
                representation.

            name
                The name of the field, or None for anonymous fields.

            artificial
                This is True if the field is artificial, usually meaning that it was provided by the compiler and not
                the user. This attribute is always provided, and is False if the field is not artificial.

            is_base_class
                This is True if the field represents a base class of a C++ structure. This attribute is always provided,
                and is False if the field is not a base class of the type that is the argument of fields, or if that
                type was not a C++ class.

            bitsize
                If the field is packed, or is a bitfield, then this will have a non-zero value, which is the size of the
                field in bits. Otherwise, this will be zero; in this case the field’s size is given by its type.

            type
                The type of the field. This is usually an instance of Type, but it can be None in some situations.

            parent_type
                The type which contains this field. This is an instance of gdb.Type.
        """
        #todo implement gdb.Fields
        raise NotImplementedError

