from gdbstub.type import Type
from gdbstub.symbol import Symbol

import gdbstub.exceptions as execeptions
from gdbstub.exceptions import error

def lookup_global_symbol(name, domain=None):
    """returns a gdb.Symbol object or None if the symbol is not found

    This function searches for a global symbol by name. The search scope can be restricted to by the domain argument.
    name is the name of the symbol. It must be a string.
    The optional domain argument restricts the search to the domain type.
    The domain argument must be a domain constant defined in the gdb module and described later in this chapter.
    The result is a gdb.Symbol object or None if the symbol is not found.
    """
    return execeptions.fail_if_used(Symbol())

def lookup_symbol(name, block=None, domain=None):
    """ returns a tuple of two elements, with the symbol as the first element if found

    This function searches for a symbol by name.
    The search scope can be restricted to the parameters defined in the optional domain and block arguments.
    name is the name of the symbol. It must be a string. The optional block argument restricts the search to symbols visible in that block. The block argument must be a gdb.Block object.
    If omitted, the block for the current frame is used. The optional domain argument restricts the search to the domain type.
    The domain argument must be a domain constant defined in the gdb module and described later in this chapter.
    The result is a tuple of two elements. The first element is a gdb.Symbol object or None if the symbol is not found.
    If the symbol is found, the second element is True if the symbol is a field of a method’s object (e.g., this in C++),
    otherwise it is False. If the symbol is not found, the second element is False.
    """
    return execeptions.fail_if_used((None, None))

def execute(command, from_tty, to_string):
    """Evaluate command, a string, as a GDB CLI command.

    The from_tty flag specifies whether GDB ought to consider this command as having originated from the user invoking it interactively.
    It must be a boolean value. If omitted, it defaults to False. By default, any output produced by command is sent to GDB’s standard output
    (and to the log output if logging is turned on). If the to_string parameter is True, then output will be collected by gdb.execute
    and returned as a string.
    """
    return execeptions.fail_if_used('')

def lookup_type (name, block=None):
    """returns gdb.Type object if found, else throws exception

    This function looks up a type by its name, which must be a string.
    If block is given, then name is looked up in that scope. Otherwise, it is searched for globally.
    Ordinarily, this function will return an instance of gdb.Type. If the named type cannot be found, it will throw an exception.
    """
    return execeptions.fail_if_used(Type())


def inferiors():
    """Return a tuple containing all inferior objects."""
    pass


def selected_inferior():
    """Return an object representing the current inferior."""
    pass
