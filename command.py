
class Command:
    """
    You can implement new GDB CLI commands in Python. A CLI command is implemented using an instance of the
    gdb.Command class, most commonly using a subclass. When a new command is registered, it must be declared as a member
    of some general class of commands defined in #gdb
    """
    def __init__(self, name, command_class, completer_class=None, prefix=None):
        """
        The object initializer for Command registers the new command with GDB. This initializer is normally invoked
        from the subclass’ own __init__ method.

        name is the name of the command. If name consists of multiple words, then the initial words are looked for as
        prefix commands. In this case, if one of the prefix commands does not exist, an exception is raised.

        There is no support for multi-line commands.

        command_class should be one of the ‘COMMAND_’ constants defined below. This argument tells GDB how to
        categorize the new command in the help system.

        completer_class is an optional argument. If given, it should be one of the ‘COMPLETE_’ constants defined
        below. This argument tells GDB how to perform completion for this command. If not given, GDB will attempt to
        complete using the object’s complete method (see below); if no such method is found, an error will occur when completion is attempted.

        prefix is an optional argument. If True, then the new command is a prefix command; sub-commands of this command
        may be registered.

        The help text for the new command is taken from the Python documentation string for the command’s class, if
        there is one. If no documentation string is provided, the default value “This command is not documented.” is
        used.
        """
        def dont_repeat(self):
            """
            By default, a GDB command is repeated when the user enters a blank line at the command prompt. A command can
            suppress this behavior by invoking the dont_repeat method. This is similar to the user command dont-repeat,
            see dont-repeat.
            """

        def invoke(argument, from_tty):
            """This method is called by GDB when this command is invoked.

            argument is a string. It is the argument to the command, after leading and trailing whitespace has been stripped.

            from_tty is a boolean argument. When true, this means that the command was entered by the user at the
            terminal; when false it means that the command came from elsewhere.

            If this method throws an exception, it is turned into a GDB error call. Otherwise, the return value is
            ignored.

            To break argument up into an argv-like string use gdb.string_to_argv. This function behaves identically to
            GDB’s internal argument lexer buildargv. It is recommended to use this for consistency.
            Arguments are separated by spaces and may be quoted.
            Example:
                print gdb.string_to_argv ("1 2\ \\\"3 '4 \"5' \"6 '7\"")
                ['1', '2 "3', '4 "5', "6 '7"]"""


        def complete(text, word):
            """
            This method is called by GDB when the user attempts completion on this command. All forms of completion are
            handled by this method, that is, the TAB and M-? key bindings (see Completion), and the complete command
            (see complete).

            The arguments text and word are both strings; text holds the complete command line up to the cursor’s
            location, while word holds the last word of the command line; this is computed using a word-breaking heuristic.

            The complete method can return several values:
              - If the return value is a sequence, the contents of the sequence are used as the completions. It is up to
                complete to ensure that the contents actually do complete the word. A zero-length sequence is allowed, it
                means that there were no completions available. Only string elements of the sequence are used; other elements
                in the sequence are ignored.
              - If the return value is one of the ‘COMPLETE_’ constants defined below, then the corresponding GDB-internal
                completion function is invoked, and its result is used.
              - All other results are treated as though there were no available completions.
            """