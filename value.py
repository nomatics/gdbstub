from gdbstub import exceptions

class Value:
    """
    GDB provides values it obtains from the inferior program in an object of type gdb.Value. GDB uses this object for its
    internal bookkeeping of the inferior’s values, and for fetching values when necessary.

    Inferior values that are simple scalars can be used directly in Python expressions that are valid for the value’s data type.
    Here’s an example for an integer or floating-point value some_val:
        bar = some_val + 2

    As result of this, bar will also be a gdb.Value object whose values are of the same type as those of some_val.
    Valid Python operations can also be performed on gdb.Value objects representing a struct or class object.
    For such cases, the overloaded operator (if present), is used to perform the operation. For example, if val1
    and val2 are gdb.Value objects representing instances of a class which overloads the + operator, then one can use
    the + operator in their Python script as follows:
        val3 = val1 + val2

    The result of the operation val3 is also a gdb.Value object corresponding to the value returned by the overloaded + operator.
    In general, overloaded operators are invoked for the following operations:
        + (binary addition), - (binary subtraction), * (multiplication), /, %, <<, >>, |, &, ^.

    Inferior values that are structures or instances of some class can be accessed using the Python dictionary syntax.
    For example, if some_val is a gdb.Value instance holding a structure, you can access its foo element with:
        bar = some_val['foo']

    Again, bar will also be a gdb.Value object. Structure elements can also be accessed by using gdb.Field objects as
    subscripts (see Types In Python, for more information on gdb.Field objects). For example, if foo_field is a
    gdb.Field object corresponding to element foo of the above structure, then bar can also be accessed as follows:
        bar = some_val[foo_field]

    A gdb.Value that represents a function can be executed via inferior function call. Any arguments provided to the
    call must match the function’s prototype, and must be provided in the order specified by that prototype.

    For example, some_val is a gdb.Value instance representing a function that takes two integers as arguments.
    To execute this function, call it like so:
        result = some_val (10,20)

    Any values returned from a function call will be stored as a gdb.Value.
    """
    def __init__(self, value, address=None, is_optimized_out=None, type=None):
        """
        The following types can by converted directly to a gdb.Value object: bool, int, long, float, string,
        gdb.Value, gdb.LazyString
        """
        self.value = value
        """
        If this object is addressable, this read-only attribute holds a gdb.Value object representing the address. 
        Otherwise, this attribute holds None
        """
        self.address = address

        """
        This read-only boolean attribute is true if the compiler optimized out this value, thus it is not available 
        for fetching from the inferior.
        """
        self.is_optimized_out = is_optimized_out

        """The type of this gdb.Value. The value of this attribute is a gdb.Type object (see Types In Python)."""
        self.type = type

    def cast(self, type):
        """Return a new instance of gdb.Value that is the result of casting this instance to the type described by type,
         which must be a gdb.Type object. If the cast cannot be performed for some reason, this method throws an exception.
         """
        return exceptions.fail_if_used(Value())

    def dereference(self):
        """For pointer data types, this method returns a new gdb.Value object whose contents is the object pointed to
        by the pointer. For example, if foo is a C pointer to an int, declared in your C program as
           int *foo;

        then you can use the corresponding gdb.Value to access what foo points to like this:
           bar = foo.dereference ()

        The result bar will be a gdb.Value object holding the value pointed to by foo.

        A similar function Value.referenced_value exists which also returns gdb.Value objects corresonding to the values
        pointed to by pointer values (and additionally, values referenced by reference values). However, the behavior of
        Value.dereference differs from Value.referenced_value by the fact that the behavior of Value.dereference is
        identical to applying the C unary operator * on a given value. For example, consider a reference to a pointer
        ptrref, declared in your C++ program as
            typedef int *intptr;
                ...
            int val = 10;
            intptr ptr = &val;
            intptr &ptrref = ptr;

        Though ptrref is a reference value, one can apply the method Value.dereference to the gdb.Value object
        corresponding to it and obtain a gdb.Value which is identical to that corresponding to val. However, if you
        apply the method Value.referenced_value, the result would be a gdb.Value object identical to that corresponding
        to ptr.

        py_ptrref = gdb.parse_and_eval ("ptrref")
        py_val = py_ptrref.dereference ()
        py_ptr = py_ptrref.referenced_value ()

        The gdb.Value object py_val is identical to that corresponding to val, and py_ptr is identical to that
        corresponding to ptr. In general, Value.dereference can be applied whenever the C unary operator * can be
        applied to the corresponding C value. For those cases where applying both Value.dereference and Value.referenced
        _value is allowed, the results obtained need not be identical (as we have seen in the above example).
        The results are however identical when applied on gdb.Value objects corresponding to pointers
        (gdb.Value objects with type code TYPE_CODE_PTR) in a C/C++ program.
        """
        return exceptions.fail_if_used(Value())

    def referenced_value(self):
        """For pointer or reference data types, this method returns a new gdb.Value object corresponding to the value
        referenced by the pointer/reference value. For pointer data types, Value.dereference and Value.referenced_value
        produce identical results. The difference between these methods is that Value.dereference cannot get the values
        referenced by reference values. For example, consider a reference to an int, declared in your C++ program as
            int val = 10;
            int &ref = val;

        then applying Value.dereference to the gdb.Value object corresponding to ref will result in an error, while applying
        Value.referenced_value will result in a gdb.Value object identical to that corresponding to val.
            py_ref = gdb.parse_and_eval ("ref")
            er_ref = py_ref.dereference ()       # Results in error
            py_val = py_ref.referenced_value ()  # Returns the referenced value

        The gdb.Value object py_val is identical to that corresponding to val.
        """
        return exceptions.fail_if_used(Value())

    def reference_value(self):
        """Return a gdb.Value object which is a reference to the value encapsulated by this instance."""
        return exceptions.fail_if_used(Value())

    def const_value(self):
        """Return a gdb.Value object which is a const version of the value encapsulated by this instance."""
        return exceptions.fail_if_used(Value())

    def dynamic_cast(self, type):
        """Like Value.cast, but works as if the C++ dynamic_cast operator were used. Consult a C++ reference for details."""
        return exceptions.fail_if_used(Value())

    def reinterpret_cast(self, type):
        """Like Value.cast, but works as if the C++ reinterpret_cast operator were used. Consult a C++ reference for details."""
        return exceptions.fail_if_used(Value())

    def string(self, encoding=None, errors=None, length=None):
        """If this gdb.Value represents a string, then this method converts the contents to a Python string. Otherwise,
        this method will throw an exception.

        Values are interpreted as strings according to the rules of the current language. If the optional length
        argument is given, the string will be converted to that length, and will include any embedded zeroes that the
        string may contain. Otherwise, for languages where the string is zero-terminated, the entire string will be
        converted.

        For example, in C-like languages, a value is a string if it is a pointer to or an array of characters or ints
        of type wchar_t, char16_t, or char32_t.

        If the optional encoding argument is given, it must be a string naming the encoding of the string in the
        gdb.Value, such as "ascii", "iso-8859-6" or "utf-8". It accepts the same encodings as the corresponding
        argument to Python’s string.decode method, and the Python codec machinery will be used to convert the string.
        If encoding is not given, or if encoding is the empty string, then either the target-charset
        (see Character Sets) will be used, or a language-specific encoding will be used, if the current language is
        able to supply one.

        The optional errors argument is the same as the corresponding argument to Python’s string.decode method.
        If the optional length argument is given, the string will be fetched and converted to the given length.
        """
        return exceptions.fail_if_used('')


    def lazy_string(self, encoding, length):
        """If this gdb.Value represents a string, then this method converts the contents to a gdb.LazyString
        (see Lazy Strings In Python). Otherwise, this method will throw an exception.

        If the optional encoding argument is given, it must be a string naming the encoding of the gdb.LazyString.
        Some examples are: ‘ascii’, ‘iso-8859-6’ or ‘utf-8’. If the encoding argument is an encoding that GDB does
        recognize, GDB will raise an error.

        When a lazy string is printed, the GDB encoding machinery is used to convert the string during printing.
        If the optional encoding argument is not provided, or is an empty string, GDB will automatically select the
        encoding most suitable for the string type. For further information on encoding in GDB please see Character Sets.

        If the optional length argument is given, the string will be fetched and encoded to the length of characters
        specified. If the length argument is not provided, the string will be fetched and encoded until a null of
        appropriate width is found.
        """
        #todo implement lazystring
        return exceptions.fail_if_used("")

    def fetch_lazy(self):
        """If the gdb.Value object is currently a lazy value (gdb.Value.is_lazy is True), then the value is fetched
        from the inferior. Any errors that occur in the process will produce a Python exception.

        If the gdb.Value object is not a lazy value, this method has no effect.
        This method does not return a value
        """
        return

    def __str__(self):
        return str(self.value)

    def __int__(self):
        return int(self.value)

    def __eq__(self, arg):
        return self.value == arg







