

class Inferior:
    """
    Programs which are being run under GDB are called inferiors (see Inferiors and Programs). Python scripts can
    access information about and manipulate inferiors controlled by GDB via objects of the gdb.Inferior class.
    """

    def __init__(self, num=None, pid=None, was_attached=None):
        """gdb.Inferior cannot be instanced"""

        """ID of inferior, as assigned by GDB."""
        self.num = num

        """Process ID of the inferior, as assigned by the underlying operating system."""
        self.pid = pid

        """Boolean signaling whether the inferior was created using ‘attach’, or started by GDB itself."""
        self.was_attached = was_attached

    def is_valid(self):
        """
        Returns True if the gdb.Inferior object is valid, False if not. A gdb.Inferior object will become invalid
        if the inferior no longer exists within GDB. All other gdb.Inferior methods will throw an exception if it is
        invalid at the time the method is called.
        """
        pass

    def threads(self):
        """
        This method returns a tuple holding all the threads which are valid when it is called. If there are no
        valid threads, the method will return an empty tuple.
        """
        pass

    def read_memory(self, address, length):
        """Read length addressable memory units from the inferior, starting at address. Returns a buffer object,
         which behaves much like an array or a string. It can be modified and given to the Inferior.write_memory
         function. In Python 3, the return value is a memoryview object.
        """
        pass

    def write_memory(self, address, buffer, length=None):
        """Write the contents of buffer to the inferior, starting at address. The buffer parameter must be a Python
        object which supports the buffer protocol, i.e., a string, an array or the object returned from
        Inferior.read_memory. If given, length determines the number of addressable memory units from buffer to be written.
        """
        pass

    def search_memory(self, address, length, pattern):
        """Search a region of the inferior memory starting at address with the given length using the search pattern
        supplied in pattern. The pattern parameter must be a Python object which supports the buffer protocol,
        i.e., a string, an array or the object returned from gdb.read_memory. Returns a Python Long containing the
        address where the pattern was found, or None if the pattern could not be found.
        """
        pass

    def thread_from_thread_handle(self, thread_handle):
        """
        Return the thread object corresponding to thread_handle, a thread library specific data structure such as
        pthread_t for pthreads library implementations.
        """
        pass